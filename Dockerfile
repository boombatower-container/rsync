FROM opensuse/leap

RUN zypper ref --build-only && \
    zypper -n in rsync openssh-clients && \
    zypper clean --all
